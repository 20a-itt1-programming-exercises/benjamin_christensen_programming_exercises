"""Exercise 3: Write a program to prompt the user for hours and rate per
hour to compute gross pay."""


hours=int(input('Hello, how many hours u work? '))
rate=float(input('At what rate you get payed? '))       # input angeivelse sættes allerede her.
WithoutTaxes=round(hours*rate, 2)                       # round(, 2) tilføjer ekstra decimaler
AfterTaxes=round(((WithoutTaxes)*0.92)*0.61, 2)
print('Without taxes =', WithoutTaxes, '\n'             # \n laver line break, \ gør at du kan skrive koden videre på næstet linje.
                                       'After taxes =', AfterTaxes, '\n'
                                                                    'Not too shabby :D')

