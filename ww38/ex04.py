"""Exer cise 4: Assume that we execute the following assignment state-
ments


width = 17
height = 12.0

For each of the following expressions, write the value of the expression and the
type (of the value of the expression)."""

at first width and weight = variables

1. width//2   #quotient = 8 (8*2 =16) type=int , remainder = 1 (17-16=1) type(int)
2. width/2.0  #2.0 is a floating point, the decimal indicates that, without a decimal the filetype would
              #have been an integere, the floating point = ---> 17/ 2.0 = 8,5  
3. height/3   #variable 3(int) filetype 12.0(floating point) 12.0/3= 4.0
4. 1 + 2 * 5  #variables 1+2*5 = 11 --> "rule of operations" * first after +
Use the Python interpreter to check your answers.
