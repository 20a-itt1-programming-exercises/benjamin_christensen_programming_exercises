"""Exercise 2: Rewrite your pay program using try and except so that your
program handles non-numeric input gracefully by printing a message
and exiting the program. The following shows two executions of the program:"""


try=True

try:
    Timer = int(input('Indtast dine timer'))
    Timeløn = float(input('Indtast din timeløn'))
except:
    print('Fejl!, Indsæt venligst en talværdi')
    quit()

if Timer >40:
    løn = (Timer - 40) * Timeløn * 1.5 + 40 * Timeløn
else:
    løn = Timer * Timeløn
print(løn)
