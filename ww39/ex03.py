"""Exercise 3: Write a program to prompt for a score between 0.0 and
1.0. If the score is out of range, print an error message. If the score is
between 0.0 and 1.0, print a grade using the following table:

>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F
Enter score: 0.95
A
Enter score: perfect
Bad score
Enter score: 10.0
Bad score
Enter score: 0.75
C
Enter score: 0.5
F
Run the program repeatedly as shown above to test the various different values for
input."""

try:
    grade=float(input('Enter score between 0.0-1.0 to see ur grade --> '))
except:
    print("Enter a numeric decimal and try again")
    quit()

if grade >=1.0:
    print('You gotta insert a decimal between 0.0-1.0')
elif grade >= 0.9:
    print('Awesome! you got --> A')
elif grade >= 0.8:
    print('Not to shabby! you got --> B')
elif grade >= 0.7:
    print('Decent you got --> C')
elif grade >= 0.6:
    print('Decent you got --> D')
elif grade < 0.6:
    print("I'm Sorry you got --> F")